# README #

### What is this repository for? ###

* This is the bitbucket repository for my interactive phylogeny R software and app, viewable here: http://wgearty.shinyapps.io/mammal_phylogeny
* Version 0.4.0

### How do I get set up? ###

* To run the app locally on your own computer, replicate these commands in R:

```
#!R
library(shiny)
runURL("https://bitbucket.org/wgearty/interactive-phylogeny/get/master.zip")

```

* Dependencies: ape, shiny, graphics, rCharts

### Who do I talk to? ###

* [William Gearty](mailto:willgearty@gmail.com) (author)
* Post an issue